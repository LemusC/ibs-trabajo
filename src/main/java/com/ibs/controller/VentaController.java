package com.ibs.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.ibs.entities.Ventas;
import com.ibs.entities.Detalleventa;
import com.ibs.entities.Ventas.TipoVenta;
import com.ibs.repository.ClientesRepository;
import com.ibs.repository.ProductosRepository;
import com.ibs.services.VentaService;

@Controller
@CrossOrigin
public class VentaController {

    @Autowired
    VentaService sVenta;

    @Autowired
    ProductosRepository ipr;

    @Autowired
    ClientesRepository icr;

    public static List<Detalleventa> detalles = new ArrayList<Detalleventa>();

    @GetMapping(value = "agregarDetalle")
    @ResponseBody
    @CrossOrigin
    public Detalleventa agregarDetalle(@RequestParam int cantidad, @RequestParam Long producto) {

        final Detalleventa vp = new Detalleventa();
        vp.setCantidad(cantidad);
        vp.setProducto(sVenta.getProducto(producto));
        detalles.add(vp);
        return vp;
    }

    @GetMapping(value = "allDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public List<Detalleventa> getDetallesMemoria() {
        return detalles;
    }

    @GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getDetalles() {
        return detalles;
    }

    @GetMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object resetDetalles() {
        detalles = new ArrayList<>();
        // detalles.clear();
        return "lista reseteada";
    }

    @GetMapping(value = "save")
    @ResponseBody
    @CrossOrigin
    public Boolean save(@RequestParam int numeroFactura, @RequestParam TipoVenta tipoVenta,
            @RequestParam Long idCliente, @RequestParam Long idUsuario) {

        Ventas entity = new Ventas();

        entity.setNumeroFactura(numeroFactura);
        entity.setTipoVenta(tipoVenta);
        entity.setUsuario(sVenta.getUsuario(idUsuario));
        entity.setCliente(sVenta.getCliente(idCliente));

        for (Detalleventa detalleVenta : detalles) {
            detalleVenta.setVentas(entity);
        }
        System.out.println(detalles + "" + idCliente);

        entity.setDetalleVenta(detalles);

        try {
            sVenta.save(entity);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}

$(document).ready(inicio);
var apiCliente = "/cliente/";
var apiProductos = "/producto/";

let clientes = {
    id: 0,
    nombre: ""
};

let producto = {
    id: 0,
    nombre: "",
    //descuento: ""
};

function inicio() {
    resetDetalles();
    $("#buscarCliente").click(function () {
        $('#modalCliente').modal('show');
        listarClientes();
    });

    $("#buscarProducto").click(function () {
        $('#modalProductos').modal('show');
        listarProductos();
    });

    $("#agregarDetalle").click(agregar);

}

//MODAL AGREGAR CLIENTE A LA VENTA
function seleccionarUsuario(id) {
    $.ajax({
        url: apiCliente + id,
        method: "post",
        success: function (response) {
            $("#cliente").val(response.username);
            producto.id = response.id;
            $('#modalCliente').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function seleccionarProducto(id) {
    $.ajax({
        url: apiProductos + id,
        method: "post",
        success: function (response) {
            $("#producto").val(response.nombre);

            //var desc = response.descuento / 100;

            //$("#descuento").val(desc);
            producto.id = response.id;
            $('#modalProductos').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}


function agregar() {
    console.log(producto.id);
    $.ajax({
        url: "/venta/agregarDetalle",
        method: "Get",
        data: {
            cantidad: $("#cantidad").val(),
            producto: producto.id
        },
        success: function (response) {

            alert("Detalle agregado");
            $("#cantidad").val(0);
            $("#descuento").val(0);
            $("#producto").val("");
            producto.id = 0;
            producto.nombre = "";
            cargarDetalles();
        },
        error: function (response) {
            alert("NO SE AGREGO EL DETALLE");
        }
    });
}

function cargarDetalles() {
    $.ajax({
        url: "/venta/allDetalles",
        method: "Get",
        success: function (response) {
            $("#tDetalles").html("");
            response.forEach(i => {

                $("#tDetalles").append("" +
                    "<tr>" +
                    "<td>" + i.cantidad + "</td>" +
                    /* "<td>" + i.idProducto.nombre + "</td>" +
                    "<td>$" + i.idProducto + "</td>" +
                    "<td>" + i.idProducto + "%</td>" + */

                    "<td><button class='btn btn-danger'>eliminar</button></td>" +
                    "</tr>" +
                    "");
            });
        },
        error: function (response) {}
    });
}


function listarClientes() {
    $("#tablaClientes").DataTable({
        "ajax": {
            "url": apiCliente,
            "method": "post"
        },
        "columns": [{
            "data": "username",
            "width": "20%"
        }, {
            "data": "direccion",
            "width": "20%"
        }, {
            "data": "dui",
            "width": "20%"
        }, {
            "data": "nit",
            "width": "20%"
        }, {
            "data": "nrc",
            "width": "20%"
        }, {
            "data": "giro",
            "width": "20%"
        }, {
            "data": "telefono",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function listarProductos() {
    $("#tablaProductos").DataTable({
        "ajax": {
            "url": apiProductos,
            "method": "post"
        },
        "columns": [{
            "data": "codigo",
            "width": "5%"
        }, {
            "data": "nombre",
            "width": "20%"
        }, {
            "data": "presentacion",
            "width": "20%"
        }, {
            "data": "marca",
            "width": "20%"
        }, {
            "data": "categoria",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function resetDetalles() {
    $.ajax({
        url: "/venta/resetDetalles",
        method: "Get"
    });
}